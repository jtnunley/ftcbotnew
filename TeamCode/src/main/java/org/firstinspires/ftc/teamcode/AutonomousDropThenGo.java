package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name="Drop Then Go (PLZ USE FOR MY SAKE)")
public final class AutonomousDropThenGo extends LinearOpMode {
    Hardware Robot;

    @Override
    public void runOpMode() throws InterruptedException {
        Robot = new Hardware();
        Robot.init(hardwareMap);
        Robot.lDrive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.rDrive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.rDrive.setDirection(DcMotorSimple.Direction.REVERSE);
        Robot.liftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.chainMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        telemetry.addData("Ready","Yes");
        telemetry.update();

        waitForStart();
        //Code bulk here

        Robot.liftMotor.setPower(-0.25);
        Robot.chainMotor.setPower(-1.0);

        ElapsedTime et = new ElapsedTime();
        while (et.milliseconds() < 4600 && opModeIsActive()) {
            telemetry.addData("Lift Motor Power",-0.25);
            telemetry.addData("Chain Motor Power",1.0);
            telemetry.addData("Phase","Lowering");
            telemetry.update();
        }

        Robot.liftMotor.setPower(0.0);
        Robot.chainMotor.setPower(0.0);

        // make robot shimmy
        Robot.lDrive.setPower(-0.3);
        Robot.rDrive.setPower(0.3);
        et.reset();
        while (et.milliseconds() < 500 && opModeIsActive()) {
            telemetry.addData("Phase","Shimmying");
            telemetry.update();
        }

        // make robot stop shimmying
        Robot.lDrive.setPower(0);
        Robot.rDrive.setPower(0);
        while (et.milliseconds() < 2000 && opModeIsActive()) {
            telemetry.addData("Phase","Stopped");
            telemetry.update();
        }
        //go to the crater
        Robot.lDrive.setPower(0.5);
        Robot.rDrive.setPower(0.5);
        et.reset();
        while (et.milliseconds() < 1500 && opModeIsActive()) {
            telemetry.addData("Phase","Going Towards Crater");
            telemetry.update();

        }
        Robot.lDrive.setPower(0);
        Robot.rDrive.setPower(0);
    }
}
