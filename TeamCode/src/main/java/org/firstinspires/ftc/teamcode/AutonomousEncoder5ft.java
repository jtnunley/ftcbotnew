package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

@Autonomous(name="5 Feet Forwards")
public final class AutonomousEncoder5ft extends LinearOpMode {

    Hardware Robot;

    @Override
    public void runOpMode() throws InterruptedException {
        Robot = new Hardware();
        Robot.init(hardwareMap);
        Robot.lDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        Robot.rDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        Robot.rDrive.setDirection(DcMotorSimple.Direction.REVERSE);
        Robot.liftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.chainMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        Robot.lDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        Robot.rDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        telemetry.addData("Ready","Yes");
        telemetry.update();

        waitForStart();

        AutonomousHelper.encoderDrive(Robot.lDrive,Robot.rDrive,0.6,5*12,5*12,30,this);
    }
}
