package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

public final class Hardware {
    public DcMotor lDrive, rDrive, liftMotor, chainMotor = null;

    public Hardware() { }

    public void init(HardwareMap hmx) {
        lDrive = hmx.dcMotor.get(Constants.lDriveName);
        rDrive = hmx.dcMotor.get(Constants.rDriveName);
        liftMotor = hmx.dcMotor.get(Constants.liftMotorName);
        chainMotor = hmx.dcMotor.get(Constants.chainMotorName);
    }
}
