package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name="5 Seconds Forwards")
public final class Autonomous5Sec extends LinearOpMode {

    Hardware Robot;

    @Override
    public void runOpMode() throws InterruptedException {
        Robot = new Hardware();
        Robot.init(hardwareMap);
        Robot.lDrive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.rDrive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.rDrive.setDirection(DcMotorSimple.Direction.REVERSE);
        Robot.liftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.chainMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        telemetry.addData("Ready","Yes");
        telemetry.update();

        waitForStart();

        Robot.lDrive.setPower(-0.5);
        Robot.rDrive.setPower(-0.5);
        ElapsedTime et = new ElapsedTime();

        while (et.milliseconds() < 5000 && opModeIsActive()) {
            telemetry.addData("Time",et.milliseconds());
            telemetry.update();
        }

        Robot.lDrive.setPower(0.0);
        Robot.rDrive.setPower(0.0);
    }
}
