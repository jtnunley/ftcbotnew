package org.firstinspires.ftc.teamcode;

public final class Constants {
    public static final String lDriveName = "lDrive";
    public static final String rDriveName = "rDrive";
    public static final String liftMotorName = "liftMotor";
    public static final String chainMotorName = "chainMotor";

    //Robot steering differentials
    /*
    public static final int lDiff = 0;
    public static final int rDiff = 0;
    */
}

