package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

@TeleOp(name="Tele Op")
public class TeleOpTank extends LinearOpMode {

    Hardware Robot;

    @Override
    public void runOpMode() throws InterruptedException {
        // init robot w/o encoders
        Robot = new Hardware();
        Robot.init(hardwareMap);
        Robot.lDrive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.rDrive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.chainMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        Robot.liftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        // give msg to driver
        telemetry.addData("Ready","Yes");
        telemetry.update();

        waitForStart();

        // run until stopped

        while (opModeIsActive()) {

            // get inputs from controller
            double turn, drive, left, right;
            drive = gamepad1.left_stick_x;
            turn = -gamepad1.left_stick_y;

            // get left and right power values
            left = drive + turn;
            right = drive - turn;

            // cap left and right at +/-1.00
            if (left > 1.0) left = 1.0;
            if (right > 1.0) right = 1.0;
            if (left < -1.0) left = -1.0;
            if (right < -1.0) right = -1.0;

            // set robot at 80%
            double throttle = 0.80;
            if (gamepad1.a) throttle = 0.50;
            left *= throttle;
            right *= throttle;

            // set left and right power
            Robot.lDrive.setPower(left);
            Robot.rDrive.setPower(right);

            // set chain and lift motor power
            if (gamepad1.x) {
                Robot.liftMotor.setPower(0.1);
                Robot.chainMotor.setPower(1.0);
            }
            else if (gamepad1.b) {
                Robot.liftMotor.setPower(-0.25);
                Robot.chainMotor.setPower(-1.0);
            }
            else {
                Robot.liftMotor.setPower(0.0);
                Robot.chainMotor.setPower(0.0);
            }

            // set telemetry
            telemetry.addData("Left Power",left);
            telemetry.addData("Right Power",right);
            telemetry.update();
        }
    }
}
